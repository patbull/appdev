import os
from flask import Flask, jsonify, request, render_template
import requests
import datetime


app = Flask(__name__)


# Define the base URL for the Google Cloud Storage API
BASE_URL = "https://storage.googleapis.com/storage/v1/b/"


@app.route("/")
def index() -> str:
   return render_template("index.html")




@app.route("/metadata/<int:image_id>")
def get_image_metadata( image_id):
   # Construct the URL to retrieve metadata for the image
   bucket_name = "task2a-buckets"
   image_name = f"{image_id}.jpg"
   url = f"{BASE_URL}{bucket_name}/o/{image_name}"


   # Access token
   access_token = 'ya29.a0Ad52N39YW12MiKpwQTZCfMv2gIAnyViiKX73uAah6RoW9MjtOheqFW15B2uJiGkq61YtCOB0oN1jxoSJu6xsI5Vrv6tMXXfc-jjqMDPT5JNYRLELHXO574hviEb8TxCiDAqxVPYobEed5pv47rC7GTmm8Osj1mthn18OaCgYKAQgSARMSFQHGX2MioUktFrArl50gYlzrbNllSg0171'


   # Make the API call to retrieve metadata
   headers = {"Authorization": f"Bearer {access_token}"}
   response = requests.get(url, headers=headers)


   # Parse response and extract metadata
   if response.status_code == 200:
       metadata = parse_metadata(response.json())
       return jsonify(metadata)
   else:
       return jsonify({"error": "Failed to retrieve metadata"}), 500


def parse_metadata(response_json):
   # Parse the response JSON and extract the required metadata
   metadata = {
       "id": response_json.get("id"),
       "file_name": response_json.get("name"),
       "content_type": response_json.get("contentType"),
       "file_size": response_json.get("size"),
       "creation_time": response_json.get("timeCreated"),
       "student_id": "Your Student ID",
       "request_time": str(datetime.datetime.now())
   }
   return metadata


if __name__ == "__main__":
   app.run(debug=True)