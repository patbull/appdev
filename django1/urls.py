from django.urls import path
from django.views.generic import TemplateView 
from django1 import views

urlpatterns = [
    path("", views.view_list, name="view_list"),
    path("new", views.new_list, name="new_list"),
    path("add_item", views.add_item, name="add_item"),
]


