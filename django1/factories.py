import factory
import factory.fuzzy
from django1.models import Item
from . import models

class UserFactory(factory.django.DjangoModelFactory): 
    email="user@appdev.com"
    class Meta:
        model = models.User 
        django_get_or_create = ('email',)

class ItemFactory(factory.django.DjangoModelFactory):
    text = factory.Faker('sentence')
    owner = factory.SubFactory(UserFactory)  
    class Meta:
        model = Item
