from django.test import TestCase
from django1 import models
from django1 import factories

class TestModel(TestCase):
    def test_create_item_works(self):
        user1 = factories.UserFactory()
        i1 = factories.ItemFactory(owner=user1)
        self.assertIsInstance(i1, models.Item)
        self.assertIsNotNone(i1.text)
