from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin

from .models import Item
from django1 import models

# Register your models here.

@admin.register(models.Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('text','owner','created_at',)
    search_fields = ('text', ) 

@admin.register(models.User)
class UserAdmin(DjangoUserAdmin): 
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (
            "Personal info",
            {"fields": ("first_name", "last_name")},
        ),
        (
            "Permissions",
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                )
        }, ),
        (
            "Important dates",
            {"fields": ("last_login", "date_joined")},
        ), )
    add_fieldsets = (
        (
        None, {
                "classes": ("wide",),
                "fields": ("email", "password1", "password2"),
            },
        ), )
    list_display = (
        "email",
        "first_name",
        "last_name",
        "is_staff",
    )
    search_fields = ("email", "first_name", "last_name")
    ordering = ("email",)