import os
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import WebDriverException
import time
from django.contrib.auth import get_user_model


MAX_WAIT = 5

#Here there is a limitation with this testing
#it always assumes you want to use its own test server, which it makes available at self.live_server_url
def create_test_user(username='testuser', password='testpass'):
    user = get_user_model().objects.create_user(username=username, password=password)
    return user

def login_test_user(self, client=None):
    """Optionally use a Django test client"""
    user = create_test_user()
    if client:
        client.force_login(user)
    else:
        self.browser.get(self.live_server_url + '/login/') 
        username_field = self.browser.find_element(By.NAME, 'username')
        password_field = self.browser.find_element(By.NAME, 'password')
        username_field.send_keys('testuser')
        password_field.send_keys('testpass')
        password_field.send_keys(Keys.ENTER)


class NewVisitorTest(StaticLiveServerTestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        test_server = os.environ.get("TEST_SERVER")
        if test_server:
            self.live_server_url = "http://" + test_server
        self.login_test_user()  # Log in the test user

    def tearDown(self):
        self.browser.quit()

    def wait_for_row_in_item_table(self, row_text):
        start_time = time.time()
        while True:
            try:
                table = self.browser.find_element(By.ID, "id_item_table")
                rows = table.find_elements(By.TAG_NAME, "tr")
                self.assertIn(row_text, [row.text for row in rows])
                return
            except (NoSuchElementException, WebDriverException) as e:  # Catch potential errors
                # Here you can re-attempt login if appropriate
                if "login" in self.browser.current_url:  # Check if redirected to login
                    self.login_test_user()
                    return self.wait_for_row_in_item_table(row_text) 
                else:
                    raise  # Re-raise the exception if it's not related to login


    def test_can_start_a_todo_list(self):
        # Testing a live homepage
        self.browser.get(self.live_server_url)

        # The results: page title and header should mention to-do lists
        self.assertIn("To-Do", self.browser.title)
        header_text = self.browser.find_element(By.TAG_NAME, "h1").text
        self.assertIn("To-Do", header_text)

        # Adding a to-do item by user A
        inputbox = self.browser.find_element(By.ID, "id_new_item")
        self.assertEqual(inputbox.get_attribute("placeholder"), "Enter a to-do item")

        # Typing "I am new here" into a text box
        inputbox.send_keys("I am new here")

        # Hitting enter, the page updates, and now the page lists
        # "1: I am new here" as an item in a to-do list table
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_item_table("1: I am new here")

        # Trying to add another item.
        inputbox = self.browser.find_element(By.ID, "id_new_item")
        inputbox.send_keys("Please guide me")
        inputbox.send_keys(Keys.ENTER)

        # The page updates again, and now shows both items on the list
        self.wait_for_row_in_item_table("1: I am new here")
        self.wait_for_row_in_item_table("2: Please guide me")

    def test_layout_and_styling(self):
        # X goes to the home page,
        self.browser.get(self.live_server_url)

        # Browser window is set to a very specific size
        self.browser.set_window_size(1024, 768)

        # The input box is nicely centered
        inputbox = self.browser.find_element(By.ID, "id_new_item")
        self.assertAlmostEqual(
            inputbox.location["x"] + inputbox.size["width"] / 2,
            512,
            delta=10,
        )

        # new list and the input nicely
        # centered too
        inputbox.send_keys("testing")
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_item_table("1: testing")
        inputbox = self.browser.find_element(By.ID, "id_new_item")
        self.assertAlmostEqual(
            inputbox.location["x"] + inputbox.size["width"] / 2,
            512,
            delta=10,
        )
